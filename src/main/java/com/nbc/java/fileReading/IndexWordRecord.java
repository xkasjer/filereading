package com.nbc.java.fileReading;

import java.util.ArrayList;
import java.util.Objects;

public class IndexWordRecord {
    private String word;
    private ArrayList<Integer> indexes = new ArrayList<>();

    public IndexWordRecord(String word, Integer line) {
        this.word = word;
        indexes.add(line);
    }

    public void addNewLine(Integer line) {
        if (!indexes.contains(line)) {
            indexes.add(line);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IndexWordRecord indexWordRecord = (IndexWordRecord) o;
        return Objects.equals(word, indexWordRecord.word);
    }

    @Override
    public int hashCode() {

        return Objects.hash(word);
    }

    @Override
    public String toString() {
        return word+" exists in lines: " + indexes+"\n";
    }
}
