package com.nbc.java.fileReading;

import java.util.ArrayList;

public class IndexWordList {

    ArrayList<IndexWordRecord> indexWord = new ArrayList<>();


    public synchronized void addNewWord(String word, Integer numberLine) {
        IndexWordRecord indexObj = new IndexWordRecord(word, numberLine);
        if (indexWord.contains(indexObj)) {
            indexWord.get(indexWord.indexOf(indexObj)).addNewLine(numberLine);
        } else {
            indexWord.add(indexObj);
        }
    }

    @Override
    public String toString() {
        String string = "";
        for (IndexWordRecord index : indexWord){
            string+= index.toString()+"\n";
        }
        return string;
    }
}