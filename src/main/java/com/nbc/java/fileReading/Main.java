package com.nbc.java.fileReading;


import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Stream;

public class Main {
    public static void main(String... args) {
        long startTime = System.currentTimeMillis();

        PairMapList pairMapList = new PairMapList();
        LengthWordList lengthWordList = new LengthWordList();
        IndexWordList indexWordList = new IndexWordList();


        Stream<String> lines = null;
        try {
            lines = Files.lines(Paths.get("file.txt"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        FileReader<String> fileReader = new FileReader<>(lines);

        int threadCount = 1;
        Execute[] threads = new Execute[threadCount];

        for (int i = 0; i < threadCount; i++) {
            threads[i] = new Execute(fileReader, lengthWordList, pairMapList, indexWordList);

        }
        for (int i = 0; i < threadCount; i++) {
            threads[i].start();
        }
        for (int i = 0; i < threadCount; i++) {
            try {
                threads[i].join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }


        try {
            FileWriter fileWriter = new FileWriter("filename.txt");
            PrintWriter writer = new PrintWriter(fileWriter);

            for (String word : pairMapList.getSortedWords()) {
                writer.println(word);
            }
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        long endTime = System.currentTimeMillis();

        System.out.println("That took " + (endTime - startTime) + " milliseconds");

    }
}


class Execute extends Thread {
    private LengthWordList lengthWordList;
    private PairMapList pairMapList;
    private IndexWordList indexWordList;
    private FileReader fileReader;


    public Execute(FileReader fileReader, LengthWordList lengthWordList, PairMapList pairMapList, IndexWordList indexWordList) {
        this.pairMapList = pairMapList;
        this.lengthWordList = lengthWordList;
        this.indexWordList = indexWordList;
        this.fileReader = fileReader;


    }

    @Override
    public void run() {
        String line;
        Integer numberLine;
        while (true) {
            synchronized (fileReader) {
                line = fileReader.getLine();
                numberLine = fileReader.getCurrentNumberLine();
            }
            if (line == null) {
                break;
            }

            List<String> list = new ArrayList<>(Arrays.asList(line.toLowerCase().replaceAll("[^\\w]", " ").split(" ")));
            for (String word : list) {
                if (word.length() == 0) {
                    continue;
                }
                lengthWordList.addNewWord(word);
                pairMapList.addNewLine(word);
                indexWordList.addNewWord(word, numberLine);
            }
        }
    }
}