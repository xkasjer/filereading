package com.nbc.java.fileReading;

import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class PairMapList {
    Map<String, Integer> pairsMap = new TreeMap<>();


    public synchronized void addNewLine(String word){
        if (pairsMap.containsKey(word)) {
            pairsMap.put(word, pairsMap.get(word) + 1);
        } else {
            pairsMap.put(word, 1);
        }
    }


    public Set<String> getSortedWords() {
        return pairsMap.keySet();
    }
}
