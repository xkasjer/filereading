package com.nbc.java.fileReading;

import java.util.Iterator;
import java.util.stream.Stream;

public class FileReader<T extends String> {

    private Iterator<T> streamIterator;
    private Integer line = 0;

    public FileReader(Stream<T> stream) {

        this.streamIterator = stream.iterator();
    }

    public synchronized T getLine() {
        if (streamIterator.hasNext()) {
            line++;
            return streamIterator.next();
        }
        return null;
    }

    public Integer getCurrentNumberLine(){
        return line;
    }
}
