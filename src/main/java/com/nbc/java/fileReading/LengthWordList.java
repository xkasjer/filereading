package com.nbc.java.fileReading;

import java.util.Comparator;
import java.util.TreeSet;

public class LengthWordList {
    TreeSet<String> lengthWord = new TreeSet<>(new LengthComparator());

    public synchronized void addNewWord(String word){
        if (lengthWord.size() < 10) {
            lengthWord.add(word);
        } else if (lengthWord.last().length() < word.length()) {
            lengthWord.pollLast();
            lengthWord.add(word);
        }
    }

    public TreeSet<String> getLengthWord(){
        return lengthWord;
    }
}

class LengthComparator implements Comparator<String> {


    public int compare(String e1, String e2) {
        if (e1.length() < e2.length()) {
            return 1;
        } else {
            return -1;
        }
    }
}